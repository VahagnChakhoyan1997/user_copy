<?php

$db_dir = "/var/www/rochik/rnd/trunk/rochik/vostan.db";
$donor_db_dir = "/var/www/rochik_donor/rnd/trunk/rochik/vostan.db";

//function tryTo gets function name as argument, and variable number of parameters,
//calls function with parametest and in case of exception returns false.
function tryTo($functionName, ...$arguments) {
    try {
        return $functionName(...$arguments);
    } catch (Exception $error) {
        return false;
    }
}

function filterNodesByTitleName($nodes, $title) {
    foreach ($nodes as $node) {
        if ($node->title == $title) {
            return $node;
        }
    }

    throw new Exception('Node by given title not exists.');
}

function changeNodeID($db_dir, $originalID, $changeID) {
    $db = getConnection($db_dir);

    $sql = "SELECT * FROM nodes WHERE nodeID = $changeID;";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $exists = count($stmt->fetchAll(PDO::FETCH_OBJ)) > 0;

    if(!$exists) {
        $sql1 = "UPDATE nodes SET nodeID = $changeID WHERE nodeID = $originalID;";
        $stmt = $db->prepare($sql1);
        $stmt->execute();

        $sql2 = "UPDATE links SET linkedNodeID = $changeID WHERE linkedNodeID = $originalID;";
        $stmt = $db->prepare($sql2);
        $stmt->execute();

        $sql3 = "UPDATE links SET nodeID = $changeID WHERE nodeID = $originalID;";
        $stmt = $db->prepare($sql3);
        $stmt->execute();

        $sql4 = "UPDATE settings SET linkedNodeID = $changeID WHERE linkedNodeID = $originalID;";
        $stmt = $db->prepare($sql4);
        $stmt->execute();

        $sql5 = "UPDATE settings SET nodeID = $changeID WHERE nodeID = $originalID;";
        $stmt = $db->prepare($sql5);
        $stmt->execute();

        $db = null;
    } else {
        trigger_error('Change ID already exists.', E_USER_ERROR);
    }

    return true;
}

function deleteNodeHelper($db_dir, $nid) {
    $db = getConnection($db_dir);

    $sql1 = "DELETE FROM settings WHERE nodeID = :nid OR linkedNodeID = :nid";
    $stmt = $db->prepare($sql1);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();

    $sql2 = "DELETE FROM links WHERE nodeID = :nid OR linkedNodeID = :nid";
    $stmt = $db->prepare($sql2);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();

    $sql3 = "DELETE FROM nodes WHERE nodeID = :nid";
    $stmt = $db->prepare($sql3);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();

    $db = null;

    return true;
}

function switchNodeID ($db_dir, $nid, $newNID) {
    $db = getConnection($db_dir);

    $sql1 = "UPDATE settings SET nodeID = :newNID WHERE nodeID = :nid";
    $stmt = $db->prepare($sql1);
    $stmt->bindParam("newNID", $newNID);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();

    $sql2 = "UPDATE settings SET linkedNodeID = :newNID WHERE linkedNodeID = :nid";
    $stmt = $db->prepare($sql2);
    $stmt->bindParam("newNID", $newNID);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();

    $sql3 = "UPDATE links SET nodeID = :newNID WHERE nodeID = :nid";
    $stmt = $db->prepare($sql3);
    $stmt->bindParam("newNID", $newNID);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();

    $sql4 = "UPDATE links SET linkedNodeID = :newNID WHERE linkedNodeID = :nid";
    $stmt = $db->prepare($sql4);
    $stmt->bindParam("newNID", $newNID);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();

    return true;
}

function getConnection($db_dir) {
    $dbh = new PDO("sqlite:" . $db_dir);

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

function getNodeHelper($db_dir, $nid, $lang = "hy") {
    $db = getConnection($db_dir);
    $lang = ($lang == "en") ? "" : "_$lang";

    $sql1 = "SELECT
        n.nodeID AS 'nodeID',
        n.title$lang AS 'title',
        n.img AS 'img',
        n.txt$lang AS 'txt',
        n.script AS 'script',
        n.tags$lang AS 'tags',
        n.location AS 'location',
        n.users AS 'users',
        n.viewers AS 'viewers'
        FROM
        nodes n
        WHERE n.nodeID = :nid";

    $stmt = $db->prepare($sql1);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();
    $node = $stmt->fetchAll(PDO::FETCH_OBJ);

    if (!count($node)) {
        throw new Exception("Node not exists.");
    }

    $ret_node = $node[0];

    $db = null;

    return $ret_node;
}

function getMapNodesIDesHelper($db_dir, $rid) {
    $db = getConnection($db_dir);

    $sql = "SELECT linkedNodeID from settings WHERE nodeID = :rid";

    $stmt = $db->prepare($sql);
    $stmt->bindParam("rid", $rid);
    $stmt->execute();

    $nodes = $stmt->fetchAll(PDO::FETCH_OBJ);

    return $nodes;
}

function getNodeChildsHelper($db_dir, $rid) {
    $db = getConnection($db_dir);

    $sql = "SELECT DISTINCT linkedNodeID as 'nodeID' FROM links WHERE nodeID = :rid;"/* AND linkedNodeID IN (SELECT linkedNodeID FROM settings WHERE nodeID = :rid);"*/;

    $stmt = $db->prepare($sql);
    $stmt->bindParam("rid", $rid);
    $stmt->execute();

    $nodes = $stmt->fetchAll(PDO::FETCH_OBJ);

    return $nodes;
}

function updateNodeHelper($db_dir ,$nid, $input, $rid, $lang = "hy") {
    $db = getConnection($db_dir);

    $lang = ($lang == "en") ? "" : "_$lang";

    $sql1 = "UPDATE nodes SET title$lang = :title, img = :img, txt$lang = :txt, modified = date(), script = :script, tags$lang = :tags, users = :users, viewers = :viewers, location = :location
             WHERE nodeID = :nid;";

    $stmt = $db->prepare($sql1);
    $stmt->bindParam("nid", $nid);
    $stmt->bindParam("title", $input->title);
    $stmt->bindParam("img", $input->img);
    $stmt->bindParam("txt", $input->txt);
    $stmt->bindParam("script", $input->script);
    $stmt->bindParam("tags", $input->tags);
    $stmt->bindParam("users", $input->users);
    $stmt->bindParam("viewers", $input->viewers);
    $stmt->bindParam("location", $input->location);
    $stmt->execute();

    if ($rid) {
        $sql2 = "UPDATE settings SET
                top = :top,
                left = :left,
                width = :width,
                height = :height,
                imgWidth = :imgWidth,
                imgHeight = :imgHeight,
                imgLeft = :imgLeft,
                imgTop = :imgTop,
                titleWidth = :titleWidth,
                titleHeight = :titleHeight,
                titleLeft = :titleLeft,
                titleTop = :titleTop,
                txtWidth = :txtWidth,
                txtHeight = :txtHeight,
                txtLeft = :txtLeft,
                txtTop = :txtTop,
                titleInclude = :titleInclude,
                imgInclude = :imgInclude,
                txtInclude = :txtInclude,
                modified = date(),
                leaf = :leaf,
                carousel = :carousel
                WHERE nodeID = :rid AND linkedNodeID = :nid;";
        $stmt = $db->prepare($sql2);
        $stmt->bindParam("rid", $rid);
        $stmt->bindParam("nid", $nid);
        $stmt->bindParam("top", $input->top);
        $stmt->bindParam("left", $input->left);
        $stmt->bindParam("width", $input->width);
        $stmt->bindParam("height", $input->height);
        $stmt->bindParam("imgWidth", $input->imgWidth);
        $stmt->bindParam("imgInclude", $input->imgInclude);
        $stmt->bindParam("imgHeight", $input->imgHeight);
        $stmt->bindParam("imgLeft", $input->imgLeft);
        $stmt->bindParam("imgTop", $input->imgTop);
        $stmt->bindParam("titleWidth", $input->titleWidth);
        $stmt->bindParam("titleInclude", $input->titleInclude);
        $stmt->bindParam("titleHeight", $input->titleHeight);
        $stmt->bindParam("titleLeft", $input->titleLeft);
        $stmt->bindParam("titleTop", $input->titleTop);
        $stmt->bindParam("txtWidth", $input->txtWidth);
        $stmt->bindParam("txtHeight", $input->txtHeight);
        $stmt->bindParam("txtLeft", $input->txtLeft);
        $stmt->bindParam("txtTop", $input->txtTop);
        $stmt->bindParam("txtInclude", $input->txtInclude);
        $stmt->bindParam("leaf", $input->leaf);
        $stmt->bindParam("carousel", $input->carousel);

        $stmt->execute();
    }

    $db = null;

    return true;
}

function getNodeSettingsFromMapHelper($db_dir, $rid, $nid, $lang = "hy") {
    $db = getConnection($db_dir);

    $lang = ($lang == "en") ? "" : "_$lang";
    $sql1 = "SELECT
            n.nodeID AS 'nodeID',
            n.title$lang AS 'title',
            n.img AS 'img',
            n.txt$lang AS 'txt',
            n.script AS 'script',
            n.tags$lang AS 'tags',
            n.users AS 'users',
            n.viewers AS 'viewers',
            n.location AS 'location',
            s.top AS 'top',
            s.left AS 'left',
            s.width AS 'width',
            s.height AS 'height',
            s.imgWidth AS 'imgWidth',
            s.imgHeight AS 'imgHeight',
            s.imgLeft AS 'imgLeft',
            s.imgTop AS 'imgTop',
            s.titleWidth AS 'titleWidth',
            s.titleHeight AS 'titleHeight',
            s.titleLeft AS 'titleLeft',
            s.titleTop AS 'titleTop',
            s.txtWidth AS 'txtWidth',
            s.txtHeight AS 'txtHeight',
            s.txtLeft AS 'txtLeft',
            s.txtTop AS 'txtTop',
            s.titleInclude AS 'titleInclude',
            s.imgInclude AS 'imgInclude',
            s.txtInclude AS 'txtInclude',
            s.leaf AS 'leaf',
            s.carousel AS 'carousel'
            FROM
            settings s INNER JOIN nodes n ON n.nodeID = $nid AND s.linkedNodeID = n.nodeID
            WHERE s.nodeID = :rid";
    $stmt = $db->prepare($sql1);
    $stmt->bindParam("rid", $rid);
    $stmt->execute();
    $node = $stmt->fetchAll(PDO::FETCH_OBJ);

    if (!count($node)) {
        throw new Exception("Node not exists.");
    }

    $ret_node = $node[0];

    $db = null;

    return $ret_node;
}

function getLinkHelper($db_dir, $rid, $nid, $lang = "hy") {
    $db = getConnection($db_dir);
    $lang = ($lang == "en") ? "" : "_$lang";

    $sql1 = "SELECT
        l.nodeID AS 'nodeID',
        l.linkedNodeID AS 'linkedNodeID',
        l.tags$lang AS 'tags'
        FROM
        links l
        WHERE l.nodeID = :rid AND l.linkedNodeID = :nid";
    $stmt = $db->prepare($sql1);
    $stmt->bindParam("rid", $rid);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();
    $link = $stmt->fetchAll(PDO::FETCH_OBJ);
    $db = null;

    if (!count($link)) {
        throw new Exception("Link not exists.");
    }

    $ret_link = $link[0];

    $db = null;

    return $ret_link;
}

function addSimpleNodeHelper($db_dir, $input , $lang = "hy") {
    $db = getConnection($db_dir);

    $sql1 = "SELECT max(nodeID) FROM nodes;";
    $stmt = $db->prepare($sql1);
    $stmt->execute();
    $max_id_db = $stmt->fetch(PDO::FETCH_NUM);
    $max_id_db = $max_id_db[0];
    $max_id = $max_id_db + 1;

    $sql2 = "";
    if ($lang == "en") {
        $lang = "";
        $sql2 = "INSERT INTO nodes (nodeID, title, img, txt, modified, script, tags, users, viewers, location) VALUES ($max_id, :title, :img, :txt, date(), :script, :tags, :users, :viewers, :location);";
    } else {
        $lang = "_$lang";
        $sql2 = "INSERT INTO nodes (nodeID, title, img, txt$lang, modified, script, tags$lang, users, viewers, title$lang, location) VALUES ($max_id, 'New Node', :img, :txt, date(), :script, :tags, :users, :viewers, :title, :location);";
    }

    $stmt = $db->prepare($sql2);
    $stmt->bindParam("title", $input->title);
    $stmt->bindParam("img", $input->img);
    $stmt->bindParam("txt", $input->txt);
    $stmt->bindParam("script", $input->script);
    $stmt->bindParam("tags", $input->tags);
    $stmt->bindParam("users", $input->users);
    $stmt->bindParam("viewers", $input->viewers);
    $stmt->bindParam("location", $input->location);
    $stmt->execute();

    return $max_id;
}

function showNodeInRootMap($db_dir, $rid, $nid, $input) {
    $db = getConnection($db_dir);

    $sql1 = "INSERT INTO settings (nodeID, linkedNodeID, top, left, width, height, imgWidth, imgInclude, imgHeight, imgLeft, imgTop, titleWidth, titleInclude, titleHeight, titleLeft, titleTop, txtWidth, txtHeight, txtLeft, txtTop, txtInclude, leaf, carousel, modified)
        VALUES (:rid, :nid, :top, :left, :width, :height, :imgWidth, :imgInclude, :imgHeight, :imgLeft, :imgTop, :titleWidth, :titleInclude, :titleHeight, :titleLeft, :titleTop, :txtWidth, :txtHeight, :txtLeft, :txtTop, :txtInclude, :leaf, :carousel, date());";
    $stmt = $db->prepare($sql1);
    $stmt->bindParam("rid", $rid);
    $stmt->bindParam("nid", $nid);
    $stmt->bindParam("top", $input->top);
    $stmt->bindParam("left", $input->left);
    $stmt->bindParam("width", $input->width);
    $stmt->bindParam("height", $input->height);
    $stmt->bindParam("imgWidth", $input->imgWidth);
    $stmt->bindParam("imgInclude", $input->imgInclude);
    $stmt->bindParam("imgHeight", $input->imgHeight);
    $stmt->bindParam("imgLeft", $input->imgLeft);
    $stmt->bindParam("imgTop", $input->imgTop);
    $stmt->bindParam("titleWidth", $input->titleWidth);
    $stmt->bindParam("titleInclude", $input->titleInclude);
    $stmt->bindParam("titleHeight", $input->titleHeight);
    $stmt->bindParam("titleLeft", $input->titleLeft);
    $stmt->bindParam("titleTop", $input->titleTop);
    $stmt->bindParam("txtWidth", $input->txtWidth);
    $stmt->bindParam("txtHeight", $input->txtHeight);
    $stmt->bindParam("txtLeft", $input->txtLeft);
    $stmt->bindParam("txtTop", $input->txtTop);
    $stmt->bindParam("txtInclude", $input->txtInclude);
    $stmt->bindParam("leaf", $input->leaf);
    $stmt->bindParam("carousel", $input->carousel);

    $stmt->execute();

    $db = null;

    return true;
}

function updateLinkHelper($rid, $nid, $input, $lang = "hy") {
    $db = getConnection();

    $lang = ($lang == "en") ? "" : "_$lang";

    $sql1 = "UPDATE links SET tags$lang = :tags, modified = date()
        WHERE nodeID = :rid AND linkedNodeID = :nid;";
    $stmt = $db->prepare($sql1);
    $stmt->bindParam("rid", $rid);
    $stmt->bindParam("nid", $nid);
    $stmt->bindParam("tags", $input->tags);
    $stmt->execute();

    $db = null;

    return true;
}

function addLinkHelper($db_dir, $rid, $nid, $input, $lang = "hy") {
    $lang = ($lang == "en") ? "" : "_$lang";
    $db = getConnection($db_dir);

    $sql = "INSERT INTO links (nodeID, linkedNodeID, tags$lang, modified)
        VALUES (:rid, :nid, :tags, date());";
    $stmt = $db->prepare($sql);
    $stmt->bindParam("rid", $rid);
    $stmt->bindParam("nid", $nid);
    $stmt->bindParam("tags", $input->tags);
    $stmt->execute();

    $db = null;

    return true;
}

function addQueryCondition($query, $condition) {
    if (strlen(trim($query)) > 0 && strlen(trim($condition)) > 0) {
        $query = $query . " AND ";
    }
    $query = $query . $condition;
    return $query;
}

function getSubstr($prefix, $postfix, $text) {
    $fPos = strpos($text, $prefix);
    $lPos = strrpos($text, $postfix);
    $str = substr($text, $fPos+1, $lPos-$fPos-1);
    return trim($str);
}

function getPrefix($prefix, $text) {
    $fPos = strpos($text, $prefix);
    $str = substr($text, 0, $fPos);
    return trim($str);
}

function getQueryHelper($db_dir, $rid, $queryString, $lang = "hy") {
    $lang = ($lang == "en") ? "" : "_$lang";

    //parseBody
    $qParts = explode(",", getSubstr("(", ")", $queryString));

    $sortField = getSubstr("(", ")", $qParts[1]);
    $sortOrder = getPrefix("(", $qParts[1]);

    $tagParts = explode(".", $qParts[0]);

    $lParts = $tagParts[0];
    $lType = getPrefix("{", $lParts);

    $lTypeStatement = "(l.nodeID = :rid OR l.linkedNodeID = :rid)";
    $lJoinStatement = "l.linkedNodeID = n.nodeID or l.nodeID = n.nodeID";
    if(strtolower($lType) == "ine") {
        $lTypeStatement = "(l.linkedNodeID = :rid)";
        $lJoinStatement = "l.nodeID = n.nodeID";
    } else if(strtolower($lType) == "oute"){
        $lTypeStatement = "(l.nodeID = :rid)";
        $lJoinStatement = "l.linkedNodeID = n.nodeID";
    } else if(strtolower($lType) == "") {
        $rid = 0;
    }

    $lOperator = "AND";
    $lParts = getSubstr("{", "}", $lParts);
    if (strlen($lParts) == 0) {
        $lTags = array();
    } else {
        $lTags = explode("&", $lParts);
        if (sizeOf($lTags) == 1) {
            $lOperator = "OR";
            $lTags = explode("|", $lParts);
        }
    }

    $lTagsLength = sizeOf($lTags);
    $lTagsCondition = "";
    for ($i = 0; $i < $lTagsLength; ++$i) {
        $tag = trim($lTags[$i], '"');
        $lTagsCondition = $lTagsCondition . "(l.tags$lang LIKE '$tag' OR l.tags$lang LIKE '%'||',$tag' OR l.tags$lang LIKE '$tag,'||'%' OR l.tags$lang LIKE '%'||',$tag,'||'%')";
        if ($i != ($lTagsLength - 1)) {
            $lTagsCondition = $lTagsCondition . " " . $lOperator . " ";
        }
    }
    if (strlen($lTagsCondition) > 0) {
        $lTagsCondition = " (" . $lTagsCondition . ") ";
    }

    $nParts = $tagParts[1];
    $nOperator = "AND";
    $nParts = getSubstr("{", "}", $nParts);
    if (strlen($nParts) == 0) {
        $nTags = array();
    } else {
        $nTags = explode("&", $nParts);
        if (sizeOf($nTags) == 1) {
            $nOperator = "OR";
            $nTags = explode("|", $nParts);
        }
    }

    $nTagsLength = sizeOf($nTags);
    $nTagsCondition = "";

    for ($i = 0; $i < $nTagsLength; ++$i) {
        $tag = trim($nTags[$i], '"');
        $nTagsCondition = $nTagsCondition . "(n.tags$lang LIKE '$tag' OR n.tags$lang LIKE '%'||',$tag' OR n.tags$lang LIKE '$tag,'||'%' OR n.tags$lang LIKE '%'||',$tag,'||'%')";
        if ($i != ($nTagsLength - 1)) {
            $nTagsCondition = $nTagsCondition . " " . $nOperator . " ";
        }
    }
    if (strlen($nTagsCondition) > 0) {
        $nTagsCondition = " (" . $nTagsCondition . ") ";
    }

    $db = getConnection($db_dir);

    $columns = "n.nodeID AS 'nodeID',
                n.img AS img,
                n.title$lang AS 'title',
                n.tags$lang AS 'tags',
                n.txt$lang AS 'txt',
                n.script AS 'script',
                n.location AS 'location',
                n.users AS 'users',
                n.viewers AS 'viewers'";
    $query_condititon = "(n.tags$lang not LIKE '%Query%')";

    $query = "";
    $query = addQueryCondition($query, $nTagsCondition);
    $query = addQueryCondition($query, $query_condititon);
    if ($rid) {
        $query = addQueryCondition($query, $lTypeStatement);
        $query = addQueryCondition($query, $lTagsCondition);
        $sql = "SELECT distinct $columns FROM links l
            INNER JOIN nodes n ON $lJoinStatement
            WHERE $query ORDER BY n.$sortField $sortOrder";
        $stmt = $db->prepare($sql);
        $stmt->bindParam("rid", $rid);
    } else {
        $sql = "SELECT distinct $columns FROM nodes n
            WHERE $query ORDER BY n.$sortField $sortOrder";
        $stmt = $db->prepare($sql);
    }

    $stmt->execute();
    $nodes = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return $nodes;
}

function replaceNode($donorNode, $node) {
    global $db_dir;

    $newNodeId = addSimpleNodeHelper($db_dir, $donorNode);
    switchNodeID($db_dir, $node->nodeID, $newNodeId);
    deleteNodeHelper($db_dir, $node->nodeID);
    changeNodeID($db_dir, $newNodeId, $node->nodeID);

    return true;
}

function getParentNode($db_dir, $nid, $lang = "hy") {
    $db = getConnection($db_dir);

    $lang = ($lang == "en") ? "" : "_$lang";

    $sql = "SELECT
        n.nodeID AS 'nodeID',
        n.title$lang AS 'title',
        n.img AS 'img',
        n.txt$lang AS 'txt',
        n.script AS 'script',
        n.tags$lang AS 'tags',
        n.location AS 'location',
        n.users AS 'users',
        n.viewers AS 'viewers'
        FROM
        nodes n
        WHERE n.nodeID IN (SELECT nodeID FROM settings s WHERE s.linkedNodeID = :nid AND s.nodeID != :nid);";

    $stmt = $db->prepare($sql);
    $stmt->bindParam("nid", $nid);
    $stmt->execute();

    $nodeIDes = $stmt->fetchAll(PDO::FETCH_OBJ);

    if (!count($nodeIDes)) {
        throw new Exception("No parent node.");
    }

    return $nodeIDes[0];
}

function CheckIfQueryString($string) {
    $queryRegExp = "/vostan::query/";
    return preg_match($queryRegExp, $string);
}

function compareNodes($n1, $n2) {
    return $n1->title === $n2->title && $n1->tags === $n2->tags;
}

$donorRootId = $_GET['oldRoot'];//intval(readline("User's nodeID from old database: "));//readline's expression may be changed
$rootId = $_GET['newRoot'];//intval(readline("Users nodeID from current database: "));

if ($donorRootId !== null && $rootId !== null) {
    if (tryTo("checkExistingNode", $donorRootId, $rootId)) {
        echo "success";
    } else {
        echo "error";
    }
} else {
    echo "not valid request";
}



function handleQueryNodes($queryNodeFromOldDB, $queryNodeFromNewDB) {
    global $donor_db_dir;
    global $db_dir;

    $oldQueryNodeParent = getParentNode($donor_db_dir, $queryNodeFromOldDB->nodeID);
    $newQueryNodeParent = getParentNode($db_dir, $queryNodeFromNewDB->nodeID);

    $oldQueryNodes = getQueryHelper($donor_db_dir, $oldQueryNodeParent->nodeID, $queryNodeFromOldDB->tags);
    $newQueryNodes = getQueryHelper($db_dir, $newQueryNodeParent->nodeID, $queryNodeFromNewDB->tags);

    foreach ($oldQueryNodes as $currentOldQueryNode) {
        $exists = false;
        foreach ($newQueryNodes as $currentNewQueryNode) {
            if (compareNodes($currentOldQueryNode, $currentNewQueryNode)) {
                if (tryTo("checkExistingNode", $currentOldQueryNode->nodeID, $currentNewQueryNode->nodeID)) {
                    $exists = true;
                    break;
                }
            }
        }

        if (!$exists) {
            $newQueryNodeID = addNotExistingQueryNodeToRoot($donor_db_dir, $db_dir, $currentOldQueryNode->nodeID, $oldQueryNodeParent->nodeID, $newQueryNodeParent->nodeID);
            if ($newQueryNodeParent->img !== "") {
                $newQueryNodeParentSettings = createSettingsWithImgInclude();
            } else {
                $newQueryNodeParentSettings = createSettingsWithImgExclude();
            }

            showNodeInRootMap($db_dir, $newQueryNodeID, $newQueryNodeParent->nodeID, $newQueryNodeParentSettings);
            copySubtreeFromOldDBToNew($donor_db_dir, $db_dir, $currentOldQueryNode->nodeID, $oldQueryNodeParent->nodeID, $newQueryNodeID);
        }
    }
}

function copySubtreeFromOldDBToNew($donor_db_dir, $db_dir, $oldNodeID, $oldNodeRootID, $newNodeID) {
    $childNodesID = getNodeChildsHelper($donor_db_dir, $oldNodeID);
    foreach ($childNodesID as $currentChildNodeID) {
        if ($currentChildNodeID->nodeID != $oldNodeRootID) {
            $currentNewChildNodeID = addNotExistingNodeToRoot($donor_db_dir, $db_dir, $currentChildNodeID->nodeID, $oldNodeID, $newNodeID);
            copySubtreeFromOldDBToNew($donor_db_dir, $db_dir, $currentChildNodeID->nodeID, $oldNodeID, $currentNewChildNodeID);
        }
    }

    return true;
}

function addNotExistingQueryNodeToRoot($donor_db_dir, $db_dir, $oldNodeID, $oldNodeParentID, $newNodeParentID) {
    $node = getNodeHelper($donor_db_dir, $oldNodeID);
    $nodeSelfSettings = getNodeSettingsFromMapHelper($donor_db_dir, $oldNodeID, $oldNodeID);
    if (!$link = tryTo("getLinkHelper", $donor_db_dir, $oldNodeParentID, $oldNodeID)) {
        $link = getLinkHelper($donor_db_dir, $oldNodeID, $oldNodeParentID);
    }
    $newNodeID = addSimpleNodeHelper($db_dir, $node);
    showNodeInRootMap($db_dir, $newNodeID, $newNodeID, $nodeSelfSettings);
    addLinkHelper($db_dir, $newNodeID, $newNodeParentID, $link);

    return $newNodeID;
}

function addNotExistingNodeToRoot($donor_db_dir, $db_dir, $oldNodeID, $oldNodeParentID, $newNodeParentID) {
    $node = getNodeHelper($donor_db_dir, $oldNodeID);
    $nodeSelfSettings = getNodeSettingsFromMapHelper($donor_db_dir, $oldNodeID, $oldNodeID);
    $nodeParentSettings = getNodeSettingsFromMapHelper($donor_db_dir, $oldNodeParentID, $oldNodeID);
    if (!$link = tryTo("getLinkHelper", $donor_db_dir, $oldNodeParentID, $oldNodeID)) {
        $link = getLinkHelper($donor_db_dir, $oldNodeID, $oldNodeParentID);
    }
    $newNodeID = addSimpleNodeHelper($db_dir, $node);
    showNodeInRootMap($db_dir, $newNodeID, $newNodeID, $nodeSelfSettings);
    showNodeInRootMap($db_dir, $newNodeParentID, $newNodeID, $nodeParentSettings);
    addLinkHelper($db_dir, $newNodeParentID, $newNodeID, $link);

    return $newNodeID;
}

function checkExistingNode($idFromOldDB, $idFromNewDB) {
    global $db_dir;
    global $donor_db_dir;

    $nodeFromOldDB = getNodeHelper($donor_db_dir, $idFromOldDB);
    $nodeFromNewDB = getNodeHelper($db_dir, $idFromNewDB);

    if (compareNodes($nodeFromOldDB, $nodeFromNewDB)) {
        if (($oldNodeChilds = tryTo("getNodeChildsHelper", $donor_db_dir, $idFromOldDB)) && ($newNodeChilds = tryTo("getNodeChildsHelper", $db_dir, $idFromNewDB))) {
            foreach ($oldNodeChilds as $currentOldNodeChild) {
                foreach ($newNodeChilds as $currentNewNodeChild) {
                    if (tryTo("checkExistingNode", $currentOldNodeChild->nodeID, $currentNewNodeChild->nodeID)) {
                        break;
                    }
                }
            }
        }

        replaceNode($nodeFromOldDB, $nodeFromNewDB);

        if (CheckIfQueryString($nodeFromOldDB->tags)) {
            tryTo("handleQueryNodes", $nodeFromOldDB, $nodeFromNewDB);
        }

        return true;
    } else {
        return false;
    }
}

function createLink($nodeID, $linkedNodeID, $tags) {
    return (object)array(
        "nodeID" => $nodeID,
        "linkedNodeID" => $linkedNodeID,
        "tags" => $tags
    );
}

function setNodeSettings($titleInclude, $imgInclude, $txtInclude, $leaf, $carousel, $width, $height, $top, $left, $titleWidth, $titleHeight, $titleLeft, $titleTop, $txtWidth, $txtHeight, $txtLeft, $txtTop, $imgWidth, $imgHeight, $imgLeft, $imgTop) {
    return (object)array(
        'titleInclude' => $titleInclude,
        'imgInclude' => $imgInclude,
        'txtInclude' => $txtInclude,
        'leaf' => $leaf,
        'carousel' => $carousel,
        'width' => $width,
        'height' => $height,
        'top' => $top,
        'left' => $left,
        'titleWidth' => $titleWidth,
        'titleHeight' => $titleHeight,
        'titleLeft' => $titleLeft,
        'titleTop' => $titleTop,
        'txtWidth' => $txtWidth,
        'txtHeight' => $txtHeight,
        'txtLeft' => $txtLeft,
        'txtTop' => $txtTop,
        'imgWidth' => $imgWidth,
        'imgHeight' => $imgHeight,
        'imgLeft' => $imgLeft,
        'imgTop' => $imgTop
    );
}

function createSettingsWithImgInclude() {
    return setNodeSettings(1, 1, 1, 0, 0, 240, 100, 10, 10, 170, 80, 60, 30, 10, 10, 220, 80, 50, 50, 10, 25);
}

function createSettingsWithImgExclude() {
    return setNodeSettings(1, 1, 1, 0, 0, 240, 100, 10, 10, 220, 80, 10, 30, 10, 10, 220, 80, 50, 50, 10, 25);
}

?>